
#!/bin/env python3
import numpy
with open('times.out', 'r') as f:
    times = [float(x.split()[0]) for x in f]

print("Standerd Deavation")
print(numpy.std(times))
print("Avrage")
print(numpy.mean(times))
