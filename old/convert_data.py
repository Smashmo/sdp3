#!/bin/env python2.7
import pickle
import pprint as pp
def main():
    with open('citiesAndDistances.pickled', 'r') as f:
        citys = pickle.load(f)
    city = citys[0]
    city_waights = citys[1]
    citys = []
    citys.append(city)
    citys.append(city_waights)
    
    with open('citysAndDistancesList.pickled', 'w+') as f:
        f.write(pickle.dumps(citys))



if __name__ == "__main__":
    main()
