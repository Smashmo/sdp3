import unittest
import travaling_sailsman
import tsp.Work as Work

class TestTSP(unittest.TestCase):
    def setUp(self):
        num_nodes = 10
        num_ants = 20
        num_itter = 12
        Work.seed = 0.5 # if changeing the seed it will chaing the asseration.

        city_data = travaling_sailsman.parse_citys_file(open_city_file(args.CitysFile), num_nodes)
        graph = Graph.GraphBit(num_nodes, city_matrix)
        self.bpv = None
        self.bpc = sys.maxint
        workers = BigGroup.BigGroup(graph, num_ants, num_itter)

        workers.start()
        if workers.bpc < self.bpc:
            self.bpv = workers.bpv
            self.bpc = workers.bpc

    def runTest(self):
        self.assertEqual(self.bpv, [4, 5, 3, 6, 0, 9, 8, 7, 1, 2], 'incorect final path for sead(0.5)')
        self.assertEqual(self.bpc, 5525, 'incorect path cost for seed(0.5)')

