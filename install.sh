#!/bin/env bash


if [ ! -d env ]; then
    mkdir env
fi

if [ $(/Home/s1474047/.local/bin/python2.7 -c "print 'runs'") ]; then
    TSP="/Home/s1474047/.local/bin"
    echo $TSP
    $TSP/virtualenv --python=$TSP/python2.7 env
    source env/bin/activate
    pip install numpy
    echo "Install compleate, run source env/bin/activate to enter the virtual env if (env) is not beside your prompt"
    echo "type deactivate to exit the virtual env. Use uninstall to undo this install."
else
    install=$PWD
    mkdir local
    cd local
    wget http://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
    tar zxf Python-2.7.9.tgz
    cd Python-2.7.9
    make clean
    ./configure --prefix=$install/local
    make && make install
    cd ..
    wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-12.0.7.tar.gz#md5=e08796f79d112f3bfa6653cc10840114
    tar zxf virtualenv-12.0.7.tar.gz
    cd virtualenv-12.0.7/
    ../bin/python setup.py install
    echo "Should now have python and virtual env installed sorry about the wait."
    cd ../..
    mkdir env
    ./local/bin/virtualenv -p ./local/bin/python2.7 env
    source env/bin/activate
    pip install numpy
    echo "Install compleate, run source env/bin/activate to enter the virtual env if (env) is not beside your prompt"
    echo "type deactivate to exit the virtual env. Use uninstall to undo this install."
fi
