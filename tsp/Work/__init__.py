from __future__ import print_function
import math
import random

seed = None
debug = None

class Work():
    """The work of an ant is implemented here."""
    def __init__(self, ID, start_node, colony):
        self.ID = ID
        self.grouping = colony
        self.curr_node = start_node
        self.graph = colony.graph
        self.path_vec = []
        self.path_vec.append(start_node)
        self.path_cost = 0
        self.Beta = 1.0
        self.Q0 = 0.5
        self.Rho = 0.99
        self.ntv = {}
        self.path_mat = []
        for i in range(0, self.graph.num_nodes):  # check if can be list comp
            if i != start_node:
                self.ntv[i] = i

        for i in range(0, self.graph.num_nodes): # check if can be list comp
            self.path_mat.append([0] * self.graph.num_nodes)

    # could this be simpler?
    def run(self):
        """ for a path walk the graph"""
        graph = self.graph
        while not self.end():
            new_node = self.state_transition_rule(self.curr_node)
            self.path_cost += graph.delta(self.curr_node, new_node)
            self.path_vec.append(new_node)
            self.path_mat[self.curr_node][new_node] = 1
            self.local_updating_rule(self.curr_node, new_node)
            self.curr_node = new_node
        self.path_cost += graph.delta(self.path_vec[-1], self.path_vec[0])
        self.grouping.update(self)
        self.__init__(self.ID, self.curr_node, self.grouping)

    def end(self):
        return not self.ntv

    #@profile
    def state_transition_rule(self, curr_node):
        """calculations of the path from the curent node."""
        graph = self.graph
        if seed:
            random.seed(seed)
        q = random.random()
        max_node = -1
        if q < self.Q0:
            if debug is True:
                print("Exploitation")
            max_val = -1
            val = None
            for node in self.ntv.values():
                if graph.tau(curr_node, node) == 0:
                    raise Exception("tau = 0")
                val = graph.tau(curr_node, node) * math.pow(
                    graph.etha(curr_node, node), self.Beta)
                if val > max_val:
                    max_val = val
                    max_node = node
        else:
            if debug is True:
                print("Exploration")
            sum = 0
            node = -1
            for node in self.ntv.values():
                if graph.tau(curr_node, node) == 0:
                    raise Exception("tau = 0")
                sum += graph.tau(curr_node, node) * math.pow(
                    graph.etha(curr_node, node), self.Beta)
            if sum == 0:
                raise Exception("sum = 0")
            avg = sum / len(self.ntv)
            if debug is True:
                print("avg = %s" % avg)

            for node in self.ntv.values():
                p = graph.tau(curr_node, node) * math.pow(
                    graph.etha(curr_node, node), self.Beta)
                if p > avg:
                    if debug is True:
                        print("p = %s" % p)
                    max_node = node
            if max_node == -1:
                max_node = node
        if max_node < 0:
            raise Exception("max_node < 0")
        del self.ntv[max_node]
        return max_node

    def local_updating_rule(self, curr_node, next_node):
        """Update the pheromones on the tau matrix to represent transitions
        of the ants"""
        graph = self.graph
        val = (1 - self.Rho) * graph.tau(
            curr_node, next_node) + (self.Rho * graph.tau0)
        graph.update_tau(curr_node, next_node, val)
