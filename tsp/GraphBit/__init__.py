from __future__ import print_function

debug = None

class GraphBit:
    """atrabutes of the paths in the graph"""
    def __init__(self, num_nodes, delta_mat, tau_mat=None):
        self.num_nodes = num_nodes
        self.delta_mat = delta_mat
        if tau_mat is None:
            self.tau_mat = []
            for i in range(0, num_nodes):
                self.tau_mat.append([0] * num_nodes)

    def delta(self, r, s):
        return self.delta_mat[r][s]

    def tau(self, r, s):
        return self.tau_mat[r][s]

    def etha(self, r, s):
        return 1.0 / self.delta(r, s)

    def update_tau(self, r, s, val):
        self.tau_mat[r][s] = val

    def reset_tau(self):
        avg = self.average_delta()
        self.tau0 = 1.0 / (self.num_nodes * 0.5 * avg)
        if debug is True:
            print("Average = %s" % (avg,))
            print("Tau0 = %s" % (self.tau0))

        for r in range(0, self.num_nodes):
            for s in range(0, self.num_nodes):
                self.tau_mat[r][s] = self.tau0

    def average_delta(self):
        return self.average(self.delta_mat)

    def average_tau(self):
        return self.average(self.tau_mat)

    def average(self, matrix):
        sum = 0
        for r in range(0, self.num_nodes):
            for s in range(0, self.num_nodes):
                sum += matrix[r][s]

        return sum / (self.num_nodes * self.num_nodes)
