# SDp3

This is the repository for the Software Develepment part 3 requierment.


### Install

clone the repostoy to a wrigtable directory.
```sh
$ git clone https://github.com/AJMKing/SDp3
```

in this directory run

```sh
$ ./instal.sh
```

This will first check the presence of the interprator python2.7 and use this to run the travaling sailsman program.
If this fails then the script will download the requierd files. This will take time as NFS in the CPLab is slow.

to run the travaling sailsman program first make sure you have an env directory and do:
```sh
$ source env/bin/activate
```

Your pprompt should look somthing like this
```sh
(env)(foo@lab1:SDp3)$
```

now you can run:
```sh
$ python travaling_sailsman -n 10 citysAndDistancesList.pickled foo.pickled
```

###Using travaling_sailsman.py

the program requiers the citysAndDistancesList.pickled and an output that you can namy what you wish.
options include

 - -n number of citys to traverce
 - -v print satments for debuging
