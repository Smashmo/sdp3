#!/bin/env python2.7
from __future__ import print_function
import sys
import pickle
import traceback
import argparse
import tsp.BigGroup as BigGroup
import tsp.GraphBit as Graph

#@profile
def main(args):
    """Main methord in the travaling sailsman problem.
    Sets up arguments from arg parse and caules city data to import the
    data and traval_graph to walk the graph. resutls are printed at the
    end of traval_grah."""

    if args.verbose:
        BigGroup.debug = True
        Graph.debug = True
        import tsp.Work as Work
        Work.debug = True

    if args.n:
        num_nodes = int(args.n)
    else:
        num_nodes = 10

    if num_nodes <= 10:
        num_ants = 20
        num_itter = 12
    else:
        num_ants = 28
        num_itter = 20

    city_data = parse_citys_file(open_city_file(args.CitysFile), num_nodes)
    traval_graph(num_nodes, num_ants, num_itter, city_data[1], city_data[0])


def open_city_file(city_file):
    with open(city_file, 'r') as citys_file:
        city_data = pickle.load(citys_file)
    return city_data

#@profile
def parse_citys_file(city_data, num_nodes):
    """This takes the input city_data file and uses the number of nodes to
    reduce the number of city anmes and the number of paths.
    This is returned in the same datastucture and
    now has the requierd number of nodes to walk."""

    citys = city_data[0][0:num_nodes]

    if num_nodes < len(city_data[1]):
        city_paths = [[path for path in city_path[0:num_nodes]]
                       for city_path in city_data[1][0:num_nodes]]
    city_matex = []
    city_matex.append(citys)
    city_matex.append(city_paths)
    return city_matex


def traval_graph(num_nodes, num_itter, num_ants, city_matrix, cities):
    """ This is where work will be done. workers.start will be the class
    that does work, atrabutes of this class contain the results.
    the graph is also build using the list that was parse at the start. """

    try:
        graph = Graph.GraphBit(num_nodes, city_matrix)
        bpv = None
        bpc = sys.maxint
        graph.reset_tau()
        workers = BigGroup.BigGroup(graph, num_ants, num_itter)

        workers.start()
        if workers.bpc < bpc:
            bpv = workers.bpv
            bpc = workers.bpc

        print("\n------------------------------------------------------------")
        print("                     Results                                ")
        print("------------------------------------------------------------\n")
        print("Best path = %s\n" % bpv)
        city_vec = []
        for node in bpv:
            city_vec.append(cities[node])

        print("Citys = %s\n" %str(cities))
        print("Best path cost = %s" % bpc)
        results = [bpv, city_vec, bpc]
        with open(args.OutputGraph, 'w+') as out_file:
            pickle.dump(results, out_file)
    except Exception as error:
        #print("exception: " + str(error))
        traceback.print_exc()


if __name__ == "__main__":
    """See main() for detail"""
    parser = argparse.ArgumentParser(
        description='For a given graph solve the travaling sailsman problem')
    parser.add_argument('-n',
        help='Number of Citys to visit. Default is 10.')
    parser.add_argument('CitysFile')
    parser.add_argument('OutputGraph')
    parser.add_argument('-v', '--verbose', action='count',
        help='Adds extra print statment to help debug the program')
    args = parser.parse_args()

    main(args)
